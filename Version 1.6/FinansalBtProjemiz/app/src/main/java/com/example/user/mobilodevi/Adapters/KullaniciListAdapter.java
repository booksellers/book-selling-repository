package com.example.user.mobilodevi.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.mobilodevi.Entities.Kullanicilar;
import com.example.user.mobilodevi.R;

import java.util.List;

public class KullaniciListAdapter extends BaseAdapter {
    List<Kullanicilar> list;
    Context context;
    Activity activity;

    public KullaniciListAdapter(List<Kullanicilar> list, Context context , Activity actvity) {

        this.list = list;
        this.context = context;
        this.activity=actvity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view= LayoutInflater.from(context).inflate(R.layout.layout,viewGroup,false);
        LinearLayout kompleLayout;
        kompleLayout = view.findViewById(R.id.KompleLayout);
        TextView tv_sq_id=(TextView)view.findViewById(R.id.textView_Sq_ID);
        TextView tv_kullanici_adi=(TextView)view.findViewById(R.id.textViewKullaniciAdi);
        TextView tv_adsoyad=(TextView)view.findViewById(R.id.textViewAdSoyad);
        TextView tv_sehir=(TextView)view.findViewById(R.id.textViewSehir);
        TextView tv_eposta=(TextView)view.findViewById(R.id.textViewEposta);



        final int sq_id = list.get(i).getID();
        final String kullanici_adi = ""+list.get(i).getKullaniciAdi();
        final String adsoyad = ""+list.get(i).getAdSoyad();
        final String sehir = ""+list.get(i).getSehir();
        final String eposta = list.get(i).getEposta();
        tv_sq_id.setText(""+sq_id);
        tv_kullanici_adi.setText(""+kullanici_adi);
        tv_adsoyad.setText(""+adsoyad);
        tv_sehir.setText(""+sehir);
        tv_eposta.setText(""+eposta);





        return view;
    }


}
