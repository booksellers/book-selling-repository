package com.example.user.mobilodevi.Services;

import com.example.user.mobilodevi.Entities.Ilanlar;
import com.example.user.mobilodevi.Entities.Kategoriler;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IlanlarService
{

    @GET("Ilanlar/findall")
    Call<List<Ilanlar>> findAll(@Header("apikey")String apikey,
                                @Header("sifre")String sifre);


    @GET("Ilanlar/findforuser/{id}")
    Call<List<Ilanlar>> findForUser(@Query("id") int id  ,
                                    @Header("apikey")String apikey,
                                @Header("sifre")String sifre);

    @GET("Ilanlar/find/{id}")
    Call<Ilanlar> find(@Query("id") int id  ,
                           @Header("apikey")String apikey,
                           @Header("sifre")String sifre);


    @GET("Ilanlar/findforname/{Kitapad}/{kat_id}/{ilce}/{ucret}")
    Call<List<Ilanlar>> findforname(@Path("Kitapad") String kitapad  ,
                       @Path("kat_id") String kat_id  ,
                       @Path("ilce") String ilce  ,
                       @Path("ucret") String ucret  ,
                       @Header("apikey")String apikey,
                       @Header("sifre")String sifre);

    @POST("Ilanlar/create")  //EKLEME
    Call<Void> create (@Body Ilanlar product,
                       @Header("apikey")String apikey,
                       @Header("sifre")String sifre);


    @PUT("Ilanlar/update")
    Call<Void> update (@Body Ilanlar product,
                       @Header("apikey")String apikey,
                       @Header("sifre")String sifre);

    @DELETE("Ilanlar/delete/{id}")
    Call<Void> delete(@Query("id") int id ,
                      @Header("apikey")String apikey, @Header("sifre")String sifre);

    @DELETE("Ilanlar/deleteforuser/{id}")
    Call<Void> deleteforuser(@Query("id") int id ,
                      @Header("apikey")String apikey, @Header("sifre")String sifre);




}
