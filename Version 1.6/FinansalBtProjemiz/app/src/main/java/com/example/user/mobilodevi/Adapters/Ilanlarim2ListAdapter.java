package com.example.user.mobilodevi.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Ilanlar;
import com.example.user.mobilodevi.IlanDuzenleActivity;
import com.example.user.mobilodevi.IlanlarimActivity;
import com.example.user.mobilodevi.InceleActivity;
import com.example.user.mobilodevi.R;
import com.example.user.mobilodevi.Services.IlanlarService;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Ilanlarim2ListAdapter extends BaseAdapter {
    List<Ilanlar> list;
    Context context;
    Activity activity;
    ProgressDialog progress ;


    public Ilanlarim2ListAdapter(List<Ilanlar> list, Context context , Activity actvity) {


        this.list = list;
        this.context = context;
        this.activity=actvity;
        progress = new ProgressDialog(activity);
        progress.setTitle("Lütfen Bekleyiniz");
        progress.setMessage("İşlem Yapılıyor ...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
final int di = i;
        view= LayoutInflater.from(context).inflate(R.layout.ilanlarim_alt_sayfa2,viewGroup,false);
       LinearLayout kompleLayout = view.findViewById(R.id.KompleLayoutAltSayfa2);
        TextView tv_id=(TextView)view.findViewById(R.id.IlanlarimAltSayfa2TextViewSQID);
        TextView tv_kitap_adi=(TextView)view.findViewById(R.id.IlanlarimAltSayfa2TextViewKitapAdi);
       // TextView tv_kategori=(TextView)view.findViewById(R.id.IlanlarimAltSayfa2TextViewKategori);
        TextView tv_ucret=(TextView)view.findViewById(R.id.ilanlarimAltSayfa2TextViewUcret);
        TextView tv_sehir=(TextView)view.findViewById(R.id.IlanlarimAltSayfa2TextViewSehir);
        TextView tv_ilce=(TextView)view.findViewById(R.id.IlanlarimAltSayfa2TextViewIlce);
        ImageView img = view.findViewById(R.id.IlanlarimAltSayfa2ImageViewResim);
        final int sq_id = list.get(i).getID();
        final String kitap_adi =""+ list.get(i).getKitapAdi().toString();
         final String kategori = ""+ list.get(i).getAdres().toString();
        final  String sehir = ""+list.get(i).getSehir().toString();
       final String ilce =""+ list.get(i).getIlce().toString();
        final String resim = ""+list.get(i).getResim().toString();
        final String ucret = ""+list.get(i).getUcret().toString();
        tv_id.setText(""+sq_id+" NUMARALI ILAN");
       tv_kitap_adi.setText("Kitap Adı :"+kitap_adi);
       //tv_kategori.setText("İletisim : "+kategori);
       tv_sehir.setText("İl :"+sehir);
       tv_ilce.setText("İlce : "+ilce);
       tv_ucret.setText("Ucret : "+ucret.toString()+" TL");
       Picasso.with(activity.getApplicationContext()).load(resim).placeholder(R.drawable.yukleniyor).error(R.drawable.resimbulunamadi).into(img);
        kompleLayout.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {


                        Intent intent = new Intent(activity,InceleActivity.class);
                        intent.putExtra("incele_id",""+list.get(di).getID());
                        intent.putExtra("incele_kitapadi",""+list.get(di).getKitapAdi());
                        intent.putExtra("incele_kategori_id",""+list.get(di).getKategoriID());
                        intent.putExtra("incele_yayinevi",""+list.get(di).getYayinEvi());
                        intent.putExtra("incele_yazar",""+list.get(di).getYazar());
                        intent.putExtra("incele_ucret",""+list.get(di).getUcret());
                        intent.putExtra("incele_aciklama",""+list.get(di).getAciklama());
                        intent.putExtra("incele_ilansahibi_id",""+list.get(di).getKullaniciID());
                        intent.putExtra("incele_il",""+list.get(di).getSehir());
                        intent.putExtra("incele_ilce",""+list.get(di).getIlce());
                        intent.putExtra("incele_telefon",""+list.get(di).getAdres());
                        intent.putExtra("incele_resim1",""+list.get(di).getResim());
                        intent.putExtra("incele_resim2",""+list.get(di).getResim2());
                        activity.startActivity(intent);






           }
       });






        return view;
    }
}
