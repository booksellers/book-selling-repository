package com.example.user.mobilodevi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mobilodevi.Adapters.IlanlarimListAdapter;
import com.example.user.mobilodevi.Adapters.KullaniciListAdapter;
import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Ilanlar;
import com.example.user.mobilodevi.Entities.Kullanicilar;
import com.example.user.mobilodevi.Services.IlanlarService;
import com.example.user.mobilodevi.Services.KullanicilarService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IlanlarimActivity extends AppCompatActivity {



    ListView liste;
    List<Ilanlar> list;
    Session session;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilanlarim);
        tv=findViewById(R.id.textView_ilanUyari2);


        tanimla();

        list  = new ArrayList<>();

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Bilgi");
        progressDialog.setMessage("Yukleniyor");
        progressDialog.setCancelable(false);
        progressDialog.show();


        IlanlarService is = APIClient.getClient().create(IlanlarService.class);
        Call<List<Ilanlar>> bilgiList = is.findForUser( session.getPrefID() , APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());  //SERVISE ISTEK ATILIR LISTELEME  BURADA findforuser a
        bilgiList.enqueue(new Callback<List<Ilanlar>>() {
            @Override
            public void onResponse(Call<List<Ilanlar>> call, Response<List<Ilanlar>> response) {

                if(response.isSuccessful())
                {
                    list=response.body();
                    IlanlarimListAdapter adp = new IlanlarimListAdapter(list,getApplicationContext(),IlanlarimActivity.this);
                    if(adp == null)
                        Toast.makeText(IlanlarimActivity.this, "Data Bulunamadi", Toast.LENGTH_SHORT).show();
                    else
                    {

                        liste.setAdapter(adp);
                        if(liste.getCount() <= 0 )
                            tv.setText("ILAN BULUNAMADI !");
                        else
                            tv.setText("ILANLAR");

                    }
                    progressDialog.cancel();

                }
                else
                {
                    progressDialog.cancel();
                    Toast.makeText(IlanlarimActivity.this, response.message().toString() , Toast.LENGTH_SHORT).show();

                }



            }

            @Override
            public void onFailure(Call<List<Ilanlar>> call, Throwable t) {
                Toast.makeText(IlanlarimActivity.this, t.getMessage() , Toast.LENGTH_SHORT).show();

            }



        });
    }

    public void tanimla()
    {
        session=new Session(getApplicationContext());
        liste = (ListView) findViewById(R.id.listViewIlanlars);

    }




    public void geriClick(View v)

    {

        Intent intent = new Intent(IlanlarimActivity.this,AnasayfaActivity.class);
        startActivity(intent);
    }

}
