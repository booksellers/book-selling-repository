package com.example.user.mobilodevi.Services;


import com.example.user.mobilodevi.Entities.Kullanicilar;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface KullanicilarService {



    @GET("Kullanicilar/findall")
    Call<List<Kullanicilar>> findAll(@Header("apikey")String apikey,
                                     @Header("sifre")String sifre);

    @GET("Kullanicilar/find/{id}")
    Call<Kullanicilar> find(@Query("id") int id  ,
                            @Header("apikey")String apikey,
                            @Header("sifre")String sifre);

    @GET("Kullanicilar/login")
    Call<Kullanicilar> login(
            @Query("kullaniciAdi") String kullaniciAdi,
            @Query("sifre") String sifrekullanici,
            @Header("apikey")String apikey,
            @Header("sifre")String sifre);

    @POST("Kullanicilar/create")
    Call<Void> create (@Body Kullanicilar product,
                       @Header("apikey")String apikey,
                       @Header("sifre")String sifre);


    @PUT("Kullanicilar/update")
    Call<Void> update (@Body Kullanicilar product,
                       @Header("apikey")String apikey,
                       @Header("sifre")String sifre);

    @DELETE("Kullanicilar/delete/{id}")
    Call<Void> delete(@Query("id") int id ,
                      @Header("apikey")String apikey, @Header("sifre")String sifre);


}
