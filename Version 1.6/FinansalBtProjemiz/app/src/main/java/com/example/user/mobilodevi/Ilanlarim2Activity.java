package com.example.user.mobilodevi;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mobilodevi.Adapters.Ilanlarim2ListAdapter;
import com.example.user.mobilodevi.Adapters.IlanlarimListAdapter;
import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Ilanlar;
import com.example.user.mobilodevi.Entities.Kategoriler;
import com.example.user.mobilodevi.Entities.Kullanicilar;
import com.example.user.mobilodevi.Services.IlanlarService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Ilanlarim2Activity extends AppCompatActivity {


    List<Ilanlar> list;
    ListView liste;
    TextView tv;
    String kitap_adi , kategori_adi ,ilce, ucret;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilanlarim2);
        tv=findViewById(R.id.textView_ilanUyari);

        kitap_adi = getIntent().getStringExtra("ex_kitapadi").trim().toLowerCase().replaceAll("\\s","");
        if(kitap_adi.length() <=0)
            kitap_adi="all";   //apiye istek atarken all giderse tümünü getiriyor o yüzden
        kategori_adi = getIntent().getStringExtra("ex_kategori_adi").trim().toLowerCase().replaceAll("\\s","");
        if(kategori_adi.equals("1")) //1 VERİ TABANINDA TÜMÜ DEMEK
            kategori_adi="all";
        ilce = getIntent().getStringExtra("ex_ilce").trim().toLowerCase().replaceAll("\\s","");
        if(ilce.equals("tumu".toString()))
            ilce="all";
        ucret = getIntent().getStringExtra("ex_ucret").trim().toLowerCase().replaceAll("\\s","");
      //  Toast.makeText(this, kitap_adi +"....."+kategori_adi+"...."+ilce+"......."+ucret, Toast.LENGTH_LONG).show();



        list  = new ArrayList<>();
        liste=findViewById(R.id.listViewIlanlar2);


        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Bilgi");
        progressDialog.setMessage("Yükleniyor");
        progressDialog.setCancelable(false);
        progressDialog.show();


        IlanlarService is = APIClient.getClient().create(IlanlarService.class);
        Call<List<Ilanlar>> bilgiList = is.findforname( kitap_adi,kategori_adi,ilce,ucret, APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());
        bilgiList.enqueue(new Callback<List<Ilanlar>>() {
            @Override
            public void onResponse(Call<List<Ilanlar>> call, Response<List<Ilanlar>> response) {
                if(response.isSuccessful())
                {
                    list=response.body();
                    Ilanlarim2ListAdapter adp = new Ilanlarim2ListAdapter(list,getApplicationContext(),Ilanlarim2Activity.this);
                    if(adp == null)
                        Toast.makeText(Ilanlarim2Activity.this, "Data Bulunamadi", Toast.LENGTH_SHORT).show();
                    else
                    {

                        liste.setAdapter(adp);
                        if(liste.getCount() <= 0 )
                            tv.setText("ILAN BULUNAMADI !");
                        else
                            tv.setText("ILANLAR");



                    }
                    progressDialog.cancel();

                }
                else
                {
                    progressDialog.cancel();
                    Toast.makeText(Ilanlarim2Activity.this, "Hata Olustu".toString() , Toast.LENGTH_SHORT).show();

                }



            }

            @Override
            public void onFailure(Call<List<Ilanlar>> call, Throwable t) {
                Toast.makeText(Ilanlarim2Activity.this,  "Hata Olustu" , Toast.LENGTH_SHORT).show();

            }



        });




    }
}
