package com.example.user.mobilodevi.Services;

import com.example.user.mobilodevi.Entities.Kategoriler;
import com.example.user.mobilodevi.Entities.Kullanicilar;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface KategorilerService {


    @GET("Kategori/findall")
    Call<List<Kategoriler>> findAll(@Header("apikey")String apikey,
                                     @Header("sifre")String sifre);

    @GET("Kategori/find/{id}")
    Call<Kategoriler> find(@Query("id") int id  ,
                            @Header("apikey")String apikey,
                            @Header("sifre")String sifre);


    @POST("Kategori/create")
    Call<Void> create (@Body Kategoriler product,
                       @Header("apikey")String apikey,
                       @Header("sifre")String sifre);


    @PUT("Kategori/update")
    Call<Void> update (@Body Kategoriler product,
                       @Header("apikey")String apikey,
                       @Header("sifre")String sifre);

    @DELETE("Kategori/delete/{id}")
    Call<Void> delete(@Query("id") int id ,
                      @Header("apikey")String apikey, @Header("sifre")String sifre);



}
