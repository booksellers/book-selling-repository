package com.example.user.mobilodevi.Entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Kategoriler{
	@SerializedName("ID")
	private int ID;
	@SerializedName("Ilanlars")
	private List<Object> Ilanlars;
	@SerializedName("Kategori_Adi")
	private String Kategori_Adi;

	public void setID(int ID){
		this.ID = ID;
	}

	public int getID(){
		return ID;
	}

	public void setIlanlars(List<Object> Ilanlars){
		this.Ilanlars = Ilanlars;
	}

	public List<Object> getIlanlars(){
		return Ilanlars;
	}

	public void setKategoriAdi(String Kategori_Adi){
		this.Kategori_Adi = Kategori_Adi;
	}

	public String getKategoriAdi(){
		return Kategori_Adi;
	}

	@Override
 	public String toString(){
		return 
			"Kategoriler{" + 
			"iD = '" + ID + '\'' +
			",ilanlars = '" + Ilanlars + '\'' +
			",kategori_Adi = '" + Kategori_Adi + '\'' +
			"}";
		}
}