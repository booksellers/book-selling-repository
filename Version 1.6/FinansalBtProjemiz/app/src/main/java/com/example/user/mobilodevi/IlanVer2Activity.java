package com.example.user.mobilodevi;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class IlanVer2Activity extends AppCompatActivity implements View.OnClickListener{

    String durum , kitap_adi , yayin_evi,ucret , kategori_adi;
    Integer tur_id,kategori_id;
    EditText ilanVer2EditTextResim1,ilanVer2EditTextResim2;
    ImageView KitapAraImageViewLogo;
    TextView KitapAraTextViewCikis, KitapAraTextViewAdSoyad;
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilan_ver2);

        KitapAraImageViewLogo=(ImageView)findViewById(R.id.KitapAraImageViewLogo);
        KitapAraImageViewLogo.setOnClickListener(this);
        KitapAraTextViewCikis=(TextView)findViewById(R.id.KitapAraTextViewCikis);
        KitapAraTextViewCikis.setOnClickListener(this);
        session=new Session(getApplicationContext());
        KitapAraTextViewAdSoyad=(TextView)findViewById((R.id.KitapAraTextViewAdSoyad));
        KitapAraTextViewAdSoyad.setText(session.getPrefName());

      ilanVer2EditTextResim1 = findViewById(R.id.ilanVer2EditTextResim1);
      ilanVer2EditTextResim2 = findViewById(R.id.ilanVer2EditTextResim2);
        Intent intent = getIntent();
        durum = intent.getStringExtra("intent_ilanver_durum");
        kitap_adi =  intent.getStringExtra("intent_ilanver_kitap_adi");
        yayin_evi = intent.getStringExtra("intent_ilanver_yayin_evi");
        ucret  = intent.getStringExtra("intent_ilanver_ucret");
        kategori_adi  = intent.getStringExtra("intent_ilanver_kitap_kategori_adi");
        tur_id =  Integer.parseInt(intent.getStringExtra("intent_ilanver_tur_id"));
        kategori_id = Integer.parseInt(intent.getStringExtra("intent_ilanver_kitap_kategori_id"));


    }


    public void ilanverileri2(View v)
    {

        String r1= ilanVer2EditTextResim1.getText().toString() ,r2 = ilanVer2EditTextResim2.getText().toString();;
        if(r1.trim().length() <= 0)
            r1 = "resimyok";
        if(r2.trim().length() <= 0)
            r2 = "resimyok";

        Intent intent = new Intent(IlanVer2Activity.this,IlanVer3Activity.class);
        intent.putExtra("intent_ilanver_kitap_kategori_id",""+kategori_id);
        intent.putExtra("intent_ilanver_kitap_kategori_adi",""+kategori_adi);
        intent.putExtra("intent_ilanver_durum",durum);
        intent.putExtra("intent_ilanver_tur_id" ,   ""+tur_id);//Duzeltilecek
        intent.putExtra("intent_ilanver_kitap_adi",kitap_adi);
        intent.putExtra("intent_ilanver_yayin_evi",yayin_evi);
        intent.putExtra("intent_ilanver_ucret",ucret);
        intent.putExtra("intent_ilanver_resim1",r1);
        intent.putExtra("intent_ilanver_resim2",r2);

        startActivity(intent);

    }

    public void ilanvergeri2(View v)
    {
       // Intent intent = new Intent(IlanVer2Activity.this,IlanVerActivity.class);
      //  startActivity(intent);

    }

    @Override
    public void onClick(View view) {
        if(view.equals(KitapAraImageViewLogo)){
            Intent intent = new Intent(IlanVer2Activity.this,AnasayfaActivity.class);
            startActivity(intent); }
        else if(view.equals(KitapAraTextViewCikis)){
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //BENİ HATIRLAYI BOŞALTIYORUZ---------------------------------------

                            session.setPrefDurum(false);
                            session.setPrefID(0);
                            session.setPrefUsername("");
                            session.setPrefName("");
                            session.setPrefEposta("");
                            session.setPrefGuncellemeTarihi("");
                            session.setPrefIlce("");
                            session.setPrefKayitTarihi("");
                            session.setPrefRolID(0);
                            session.setPrefSehir("");
                            session.setPrefSifre("");
                            session.setPrefTelefon("");
                            //-------------------------------------------------------------
                            loginSayfasinaGit();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(IlanVer2Activity.this);
            builder.setMessage("Çıkmak İstediğinize Emin Misiniz ?").setPositiveButton("Evet", dialogClickListener)
                    .setNegativeButton("Hayır", dialogClickListener).show();

        }
    }
    private void loginSayfasinaGit() {
        Intent intent = new Intent(IlanVer2Activity.this,MainActivity.class);
        startActivity(intent);

    }
}