package com.example.user.mobilodevi;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class IlanVer3Activity extends AppCompatActivity implements View.OnClickListener{

    String durum , kitap_adi , yayin_evi,ucret , resim1 , resim2 , kategori_adi;
    Integer tur_id,kategori_id;
    EditText IlanVer3_EditText_Adres;
    Spinner ilanVer3_Spinner_Il , ilanVer3_Spinner_Ilce;
    ImageView KitapAraImageViewLogo;
    TextView KitapAraTextViewCikis, KitapAraTextViewAdSoyad;
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilan_ver3);

        KitapAraImageViewLogo=(ImageView)findViewById(R.id.KitapAraImageViewLogo);
        KitapAraImageViewLogo.setOnClickListener(this);
        KitapAraTextViewCikis=(TextView)findViewById(R.id.KitapAraTextViewCikis);
        KitapAraTextViewCikis.setOnClickListener(this);
        session=new Session(getApplicationContext());
        KitapAraTextViewAdSoyad=(TextView)findViewById((R.id.KitapAraTextViewAdSoyad));
        KitapAraTextViewAdSoyad.setText(session.getPrefName());

        IlanVer3_EditText_Adres = findViewById(R.id.IlanVer3_EditText_Adres);
        ilanVer3_Spinner_Il = findViewById(R.id.ilanVer3_Spinner_Il);
        ilanVer3_Spinner_Ilce = findViewById(R.id.ilanVer3_Spinner_Ilce);

        Intent intent = getIntent();
        durum = intent.getStringExtra("intent_ilanver_durum");
        kitap_adi =  intent.getStringExtra("intent_ilanver_kitap_adi");
        yayin_evi = intent.getStringExtra("intent_ilanver_yayin_evi");
        ucret  = intent.getStringExtra("intent_ilanver_ucret");
        tur_id =  Integer.parseInt(intent.getStringExtra("intent_ilanver_tur_id"));
        kategori_id = Integer.parseInt(intent.getStringExtra("intent_ilanver_kitap_kategori_id"));
        resim1 = intent.getStringExtra("intent_ilanver_resim1");
        resim2 = intent.getStringExtra("intent_ilanver_resim2");
        kategori_adi = intent.getStringExtra("intent_ilanver_kitap_kategori_adi");



    }


    public void ilanverileri3(View v)
    {
        if(IlanVer3_EditText_Adres.getText().toString().trim().length() <= 0)
            Toast.makeText(this, "Lutfen Telefon Numarasi Giriniz", Toast.LENGTH_SHORT).show();

        else {
            Intent intent = new Intent(IlanVer3Activity.this, IlanVer4Activity.class);
            intent.putExtra("intent_ilanver_kitap_kategori_id", "" + kategori_id);
            intent.putExtra("intent_ilanver_kitap_kategori_adi", "" + kategori_adi);
            intent.putExtra("intent_ilanver_durum", durum);
            intent.putExtra("intent_ilanver_tur_id", "" + tur_id);//Duzeltilecek
            intent.putExtra("intent_ilanver_kitap_adi", kitap_adi);
            intent.putExtra("intent_ilanver_yayin_evi", yayin_evi);
            intent.putExtra("intent_ilanver_ucret", ucret);
            intent.putExtra("intent_ilanver_resim1", resim1);
            intent.putExtra("intent_ilanver_resim2", resim2);
            intent.putExtra("intent_ilanver_adres", IlanVer3_EditText_Adres.getText().toString());
            intent.putExtra("intent_ilanver_il", ilanVer3_Spinner_Il.getSelectedItem().toString());
            intent.putExtra("intent_ilanver_ilce", ilanVer3_Spinner_Ilce.getSelectedItem().toString());

            startActivity(intent);

        }
    }

    public void ilanvergeri3(View v)
    {
       // Intent intent = new Intent(IlanVer3Activity.this,IlanVer2Activity.class);
      // startActivity(intent);

    }

    @Override
    public void onClick(View view) {
        if(view.equals(KitapAraImageViewLogo)){
            Intent intent = new Intent(IlanVer3Activity.this,AnasayfaActivity.class);
            startActivity(intent); }
        else if(view.equals(KitapAraTextViewCikis)){
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //BENİ HATIRLAYI BOŞALTIYORUZ---------------------------------------

                            session.setPrefDurum(false);
                            session.setPrefID(0);
                            session.setPrefUsername("");
                            session.setPrefName("");
                            session.setPrefEposta("");
                            session.setPrefGuncellemeTarihi("");
                            session.setPrefIlce("");
                            session.setPrefKayitTarihi("");
                            session.setPrefRolID(0);
                            session.setPrefSehir("");
                            session.setPrefSifre("");
                            session.setPrefTelefon("");
                            //-------------------------------------------------------------
                            loginSayfasinaGit();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(IlanVer3Activity.this);
            builder.setMessage("Çıkmak İstediğinize Emin Misiniz ?").setPositiveButton("Evet", dialogClickListener)
                    .setNegativeButton("Hayır", dialogClickListener).show();

        }
    }
    private void loginSayfasinaGit() {
        Intent intent = new Intent(IlanVer3Activity.this,MainActivity.class);
        startActivity(intent);

    }

}
