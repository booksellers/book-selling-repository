package com.example.user.mobilodevi;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {

    private SharedPreferences prefs;

    public Session(Context cntx) {
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }


    public void setPrefDurum(boolean durum) {
        prefs.edit().putBoolean("pref_durum", durum).commit();
    }

    public boolean getPrefDurum() {
        return  prefs.getBoolean("pref_durum",false);
    }
    public void setPrefID(int Id) {
        prefs.edit().putInt("pref_id", Id).commit();
    }

    public int getPrefID() {
        return  prefs.getInt("pref_id",0);
    }

    public void setPrefRolID(int rol_Id) {
        prefs.edit().putInt("pref_rolid", rol_Id).commit();
    }

    public int getPrefRolID() {
        return  prefs.getInt("pref_rolid",0);
    }



    public void setPrefUsername(String username) {
        prefs.edit().putString("pref_username", username).commit();
    }

    public String getPrefUsername() {
        return  prefs.getString("pref_username","");
    }

    public void setPrefName(String name) {
        prefs.edit().putString("pref_name", name).commit();
    }

    public String getPrefName() {
        return  prefs.getString("pref_name","");
    }


    public void setPrefEposta(String eposta) {
        prefs.edit().putString("pref_eposta", eposta).commit();
    }

    public String getPrefEposta() {
        return  prefs.getString("pref_eposta","");
    }


    public void setPrefSifre(String sifre) {
        prefs.edit().putString("pref_sifre", sifre).commit();
    }

    public String getPrefSifre() {
        return  prefs.getString("pref_sifre","");
    }


    public void setPrefIlce(String ilce) {
        prefs.edit().putString("pref_ilce", ilce).commit();
    }

    public String getPrefIlce() {
        return  prefs.getString("pref_ilce","");
    }


    public void setPrefKayitTarihi(String kayitTarihi) {
        prefs.edit().putString("pref_kayittarihi", kayitTarihi).commit();
    }

    public String getPrefKayitTarihi() {
        return  prefs.getString("pref_kayittarihi","");
    }

    public void setPrefGuncellemeTarihi(String GuncellemeTarihi) {
        prefs.edit().putString("pref_guncellemetarihi", GuncellemeTarihi).commit();
    }

    public String getPrefGuncellemeTarihi() {
        return  prefs.getString("pref_guncellemetarihi","");
    }

    public void setPrefTelefon(String telefon) {
        prefs.edit().putString("pref_telefon", telefon).commit();
    }

    public String getPrefTelefon() {
        return  prefs.getString("pref_telefon","");
    }


    public void setPrefSehir(String sehir) {
        prefs.edit().putString("pref_sehir", sehir).commit();
    }

    public String getPrefSehir() {
        return  prefs.getString("pref_sehir","");
    }










}