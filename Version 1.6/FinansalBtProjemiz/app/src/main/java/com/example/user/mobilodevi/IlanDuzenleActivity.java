package com.example.user.mobilodevi;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mobilodevi.Adapters.ViewPagerAdapter;
import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Ilanlar;
import com.example.user.mobilodevi.Entities.Kategoriler;
import com.example.user.mobilodevi.Entities.Kullanicilar;
import com.example.user.mobilodevi.Services.IlanlarService;
import com.example.user.mobilodevi.Services.KategorilerService;
import com.example.user.mobilodevi.Services.KullanicilarService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IlanDuzenleActivity extends AppCompatActivity {

    ViewPager vp;
    ViewPagerAdapter vpa;
    String[] images;
    EditText et_kitapAdi,et_Yayinevi,et_Yazar,et_Ucret,et_Aciklama,et_adres,et_resim1,et_resim2;
    TextView tv_IlanID,tv_ilanSahibi,tv_cikisyap,IlanDuzenleTextViewAdSoyad;
    Spinner sp_Kategori,sp_Il,sp_Ilce;
    int kategori_id , kullanici_id;
ImageView IlanDuzenleImageViewLogo;
    String kategori_adi , kullanici_adi , resim1,resim2;
    Button btn_Guncelle;
Session session ;
    List<String> listecik_isim;
    List<Integer> listecik_id ;


    List<Kategoriler> list;
    List <String> KategoriAdlari;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilan_duzenle);
        session=new Session(getApplicationContext());
        resim1 = getIntent().getStringExtra("duzenle_ilan_resim1");
        resim2 = getIntent().getStringExtra("duzenle_ilan_resim2");
        images = new String[]{resim1,resim2};

        vp=findViewById(R.id.IlanDuzenle_ViewPager);
        vpa = new ViewPagerAdapter(IlanDuzenleActivity.this,images);
        vp.setAdapter(vpa);
        Tanimla();
        seciliKategoriGetir();
       seciliKullaniciGetir();
       Doldur();


    }

    private void seciliKategoriGetir() {

         kategori_id = Integer.parseInt(getIntent().getStringExtra("duzenle_ilan_kategoriid"));
            KategorilerService ks = APIClient.getClient().create(KategorilerService.class);
            final Call<Kategoriler> bilgi = ks.find(kategori_id,APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Bilgi");
        progressDialog.setMessage("Yükleniyor");
        progressDialog.setCancelable(false);
        progressDialog.show();
            bilgi.enqueue(new Callback<Kategoriler>() {
                @Override
                public void onResponse(Call<Kategoriler> call, Response<Kategoriler> response) {
                    if(response.isSuccessful())
                    {
                        kategori_adi = response.body().getKategoriAdi();
                        doldurKategoriSpinner();

                    }

                    else
                    {
                    }
progressDialog.cancel();
                }

                @Override
                public void onFailure(Call<Kategoriler> call, Throwable t) {
                }
            });
progressDialog.cancel();
    }
    private void seciliKullaniciGetir() {

        kullanici_id = Integer.parseInt(getIntent().getStringExtra("duzenle_ilan_ilansahibi_id"));
        KullanicilarService ks = APIClient.getClient().create(KullanicilarService.class);
        final Call<Kullanicilar> bilgi = ks.find(kullanici_id,APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Bilgi");
        progressDialog.setMessage("Yükleniyor");
        progressDialog.setCancelable(false);
        progressDialog.show();

        bilgi.enqueue(new Callback<Kullanicilar>() {
            @Override
            public void onResponse(Call<Kullanicilar> call, Response<Kullanicilar> response) {
                if(response.isSuccessful())
                {
                    kullanici_adi = ""+response.body().getAdSoyad();
                    tv_ilanSahibi.setText(kullanici_adi.toString());

                }

                else
                {
                }
progressDialog.cancel();
            }

            @Override
            public void onFailure(Call<Kullanicilar> call, Throwable t) {
                progressDialog.cancel();

            }

        });
    }


    private void Tanimla() {

        et_kitapAdi=findViewById(R.id.IlanDuzenleEditTextKitapAdi);
        et_Yayinevi=findViewById(R.id.IlanDuzenleEditTextYayinEvi);
        et_Yazar=findViewById(R.id.IlanDuzenleEditTextYazar);
        et_Ucret=findViewById(R.id.IlanDuzenleEditTextUcret);
        et_Aciklama=findViewById(R.id.IlanDuzenleEditTextAciklama);
        et_resim1=findViewById(R.id.IlanDuzenleEditTextResim1);
        et_resim2=findViewById(R.id.IlanDuzenleEditTextResim2);
        et_adres=findViewById(R.id.IlanDuzenleEditTextTelefon);
        tv_IlanID=findViewById(R.id.ilanDuzenleTextViewIlanID);
        tv_ilanSahibi=findViewById(R.id.IlanDuzenleTextViewIlanSahibi);
        sp_Kategori=findViewById(R.id.IlanDuzenleSpinnerKategori);
        IlanDuzenleTextViewAdSoyad=findViewById(R.id.IlanDuzenleTextViewAdSoyad);
        sp_Il=findViewById(R.id.IlanDuzenleSpinnerSehir);
        sp_Ilce=findViewById(R.id.IlanDuzenleSpinnerIlce);
        IlanDuzenleImageViewLogo=findViewById(R.id.IlanDuzenleImageViewLogo);
        IlanDuzenleImageViewLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent inka = new Intent(IlanDuzenleActivity.this , AnasayfaActivity.class);
                startActivity(inka);




            }
        });

        tv_cikisyap=findViewById(R.id.IlanDuzenleTextViewCikis);






        tv_cikisyap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //BENİ HATIRLAYI BOŞALTIYORUZ---------------------------------------

                                session.setPrefDurum(false);
                                session.setPrefID(0);
                                session.setPrefUsername("");
                                session.setPrefName("");
                                session.setPrefEposta("");
                                session.setPrefGuncellemeTarihi("");
                                session.setPrefIlce("");
                                session.setPrefKayitTarihi("");
                                session.setPrefRolID(0);
                                session.setPrefSehir("");
                                session.setPrefSifre("");
                                session.setPrefTelefon("");
                                //-------------------------------------------------------------
                                Intent inko = new Intent(IlanDuzenleActivity.this , MainActivity.class);
                                startActivity(inko);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(IlanDuzenleActivity.this);
                builder.setMessage("Çıkmak İstediğinize Emin Misiniz ?").setPositiveButton("Evet", dialogClickListener)
                        .setNegativeButton("Hayır", dialogClickListener).show();






            }
        });



        btn_Guncelle = findViewById(R.id.IlanDuzenleButtonGuncelle);

        btn_Guncelle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {     //GUNCELLEME BURADA YAPILIR


                String resim1 = et_resim1.getText().toString();
                String resim2 = et_resim2.getText().toString();

                if (et_resim1.getText().toString().trim().length() <=0)
                    resim1="resimyok";
                if (et_resim2.getText().toString().trim().length() <=0)
                    resim2="resimyok";


                if (et_Yazar.getText().toString().trim().length() <=0 ||et_Yayinevi.getText().toString().trim().length() <=0 ||et_Ucret.getText().toString().trim().length()<=0

                        ||et_kitapAdi.getText().toString().trim().length()<=0 ||  et_adres.getText().toString().trim().length()<=0
                        || et_Aciklama.getText().toString().trim().length()<=0
                        ) {
                    Toast.makeText(IlanDuzenleActivity.this, "Lütfen Tum Alanlari Doldurunuz", Toast.LENGTH_SHORT).show();
                } else {


                    final ProgressDialog progressDialog = new ProgressDialog(IlanDuzenleActivity.this);
                    progressDialog.setTitle("Bilgi");
                    progressDialog.setMessage("Güncelleniyor");
                    progressDialog.setCancelable(false);


                    try {
                        Ilanlar Guncellenecek_Ilan = new Ilanlar();
                        Guncellenecek_Ilan.setYazar("" + et_Yazar.getText().toString());
                        Guncellenecek_Ilan.setID(Integer.parseInt(getIntent().getStringExtra("duzenle_ilan_id")));
                        Guncellenecek_Ilan.setYayinEvi("" + et_Yayinevi.getText().toString());
                        Guncellenecek_Ilan.setUcret("" + et_Ucret.getText().toString());
                        Guncellenecek_Ilan.setTurID(1);
                        Guncellenecek_Ilan.setSehir(
                                sp_Il.getSelectedItem().toString()
                        );
                        Guncellenecek_Ilan.setResim("" + resim1);
                        Guncellenecek_Ilan.setResim2("" +resim2);
                        Guncellenecek_Ilan.setKullaniciID(kullanici_id);
                        Guncellenecek_Ilan.setKitapAdi("" + et_kitapAdi.getText().toString());
                        Guncellenecek_Ilan.setKategoriID(listecik_id.get(sp_Kategori.getSelectedItemPosition()));
                        Guncellenecek_Ilan.setIlce(sp_Ilce.getSelectedItem().toString());
                        Guncellenecek_Ilan.setIlanTarihi("2018-01-01");
                        Guncellenecek_Ilan.setAdres("" + et_adres.getText().toString());
                        Guncellenecek_Ilan.setAciklama("" + et_Aciklama.getText().toString());


                        IlanlarService ilanlarService = APIClient.getClient().create(IlanlarService.class);
                        Call call = ilanlarService.update(Guncellenecek_Ilan, APIClient.getApiSecurityUsername(), APIClient.getApiSecurityPass());  //servise istek
                        progressDialog.show();

                        call.enqueue(new Callback() {
                            @Override
                            public void onResponse(Call call, Response response) {
                                if (response.isSuccessful()) {
                                    progressDialog.cancel();
                                    Toast.makeText(IlanDuzenleActivity.this, "Guncelleme Basarili", Toast.LENGTH_SHORT).show();


                                    Intent intent = new Intent(IlanDuzenleActivity.this, IlanlarimActivity.class);
                                    startActivity(intent);

                                } else {
                                    progressDialog.cancel();
                                    Toast.makeText(IlanDuzenleActivity.this, "Guncelleme Basarisiz", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call call, Throwable t) {
                                progressDialog.cancel();
                            }
                        });


                    } catch (Exception e) {

                    }


                }

            }
        });




    }

    private void Doldur() {

        IlanDuzenleTextViewAdSoyad.setText(session.getPrefName());
        et_kitapAdi.setText(getIntent().getStringExtra("duzenle_ilan_kitapadi"));
        et_resim1.setText(getIntent().getStringExtra("duzenle_ilan_resim1"));
        et_resim2.setText(getIntent().getStringExtra("duzenle_ilan_resim2"));
        et_Yayinevi.setText(getIntent().getStringExtra("duzenle_ilan_yayinevi"));
        et_Yazar.setText(getIntent().getStringExtra("duzenle_ilan_yazar"));
        et_Ucret.setText(getIntent().getStringExtra("duzenle_ilan_ucret"));
        et_Aciklama.setText(getIntent().getStringExtra("duzenle_ilan_aciklama"));
        et_adres.setText(getIntent().getStringExtra("duzenle_ilan_adres"));
        tv_IlanID.setText(getIntent().getStringExtra("duzenle_ilan_id"));
        doldurilceSpinner();





    }

    private void doldurilceSpinner() {

        //İLÇE SPİNNERINI DOLDUR
        String compareValue_ilce=""+getIntent().getStringExtra("duzenle_ilan_ilce").toString();
        ArrayAdapter<CharSequence> adapter_ilce = ArrayAdapter.createFromResource(this, R.array.diziIlce, android.R.layout.simple_spinner_item);
        adapter_ilce.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_Ilce.setAdapter(adapter_ilce);
        if (compareValue_ilce != null) {
            int spinnerPosition = adapter_ilce.getPosition(compareValue_ilce);
            sp_Ilce.setSelection(spinnerPosition);
        }
        else
        {
            sp_Ilce.setSelection(0);
        }



    }

    private void doldurKategoriSpinner() {





        KategorilerService ks = APIClient.getClient().create(KategorilerService.class);
        Call<List<Kategoriler>> bilgiList = ks.findAll( APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());
        bilgiList.enqueue(new Callback<List<Kategoriler>>() {
            @Override
            public void onResponse(Call<List<Kategoriler>> call, Response<List<Kategoriler>> response) {

                if(response.isSuccessful())
                {
                    list=response.body();


                    listecik_isim = new ArrayList<String>();
                    listecik_id = new ArrayList<Integer>();


                    for(int i=0;i<list.size();i++)
                    {
                        Kategoriler jObj = list.get(i);
                        listecik_isim.add(jObj.getKategoriAdi());
                        listecik_id.add(jObj.getID());


                    }
                    ArrayAdapter<String> adp1 = new ArrayAdapter<String>(IlanDuzenleActivity.this,
                            android.R.layout.simple_list_item_1, listecik_isim);
                    adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    sp_Kategori.setAdapter(adp1);
                    sp_Kategori.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    String compareValue_kategori_Adi=kategori_adi;

                    if (compareValue_kategori_Adi != null) {
                        int spinnerPositionkategori = adp1.getPosition(compareValue_kategori_Adi);
                        sp_Kategori.setSelection(spinnerPositionkategori);
                    }
                    else
                    {
                        sp_Ilce.setSelection(0);
                    }

                }
                else
                {

                }



            }

            @Override
            public void onFailure(Call<List<Kategoriler>> call, Throwable t) {

            }
        });






    }

}
