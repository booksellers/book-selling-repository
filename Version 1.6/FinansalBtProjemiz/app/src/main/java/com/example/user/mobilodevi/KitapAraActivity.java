package com.example.user.mobilodevi;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mobilodevi.Adapters.IlanlarimListAdapter;
import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Ilanlar;
import com.example.user.mobilodevi.Entities.Kategoriler;
import com.example.user.mobilodevi.Services.IlanlarService;
import com.example.user.mobilodevi.Services.KategorilerService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KitapAraActivity extends AppCompatActivity implements View.OnClickListener{
    List<Kategoriler> list;

    List<String> listecik_isim;
    List<Integer> listecik_id ;
    String kitap_adi , kategori_adi ,ilce, ucret;
    int kategori_id;
    ImageView KitapAraImageViewLogo;
    TextView KitapAraTextViewCikis, KitapAraTextViewAdSoyad;
    Button KitapAraButtonAra ;
    EditText KitapAraEditTextKitapAdi;
    CheckBox KitapAraCheckBoxUcretsiz;
            Spinner KitapAraSpinnerKategoriler,KitapAraSpinnerIller,KitapAraSpinnerIlceler;
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kitap_ara);
        KitapAraImageViewLogo=(ImageView)findViewById(R.id.KitapAraImageViewLogo);
        KitapAraImageViewLogo.setOnClickListener(this);
        KitapAraTextViewCikis=(TextView)findViewById(R.id.KitapAraTextViewCikis);
        KitapAraTextViewCikis.setOnClickListener(this);
        session=new Session(getApplicationContext());
        KitapAraTextViewAdSoyad=(TextView)findViewById((R.id.KitapAraTextViewAdSoyad));
        KitapAraTextViewAdSoyad.setText(session.getPrefName());
        KitapAraSpinnerKategoriler = findViewById(R.id.KitapAraSpinnerKategoriler);
        KitapAraSpinnerIller = findViewById(R.id.KitapAraSpinnerIller);
        KitapAraSpinnerIlceler = findViewById(R.id.KitapAraSpinnerIlceler);
        KitapAraCheckBoxUcretsiz = findViewById(R.id.KitapAraCheckBoxUcretsiz);
        KitapAraEditTextKitapAdi = findViewById(R.id.KitapAraEditTextKitapAdi);
        KitapAraButtonAra = findViewById(R.id.KitapAraButtonAra);



        KategorilereIstekAt();





        KitapAraButtonAra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


         kitap_adi= (KitapAraEditTextKitapAdi.getText().toString()) == "" ?   "all" :KitapAraEditTextKitapAdi.getText().toString();
         if(listecik_isim.get(KitapAraSpinnerKategoriler.getSelectedItemPosition()).toString() == "Tumu")
         {
             kategori_adi = "all";

         }
         else
         {
             kategori_adi = listecik_id.get(KitapAraSpinnerKategoriler.getSelectedItemPosition()).toString();

         }
         ilce = KitapAraSpinnerIlceler.getSelectedItem().toString() == "Tumu" ? "all" : KitapAraSpinnerIlceler.getSelectedItem().toString() ;

         if(KitapAraCheckBoxUcretsiz.isChecked())
         {
             ucret="0";
         }
         else
         { ucret="all";}







         AraIstekAt(kitap_adi,kategori_adi,ilce,ucret);
            }
        });
    }

    private void AraIstekAt(String kitap_adi , String kategori_adi , String ilce , String ucret) {

      Intent intent = new Intent(KitapAraActivity.this,Ilanlarim2Activity.class);
      intent.putExtra("ex_kitapadi",kitap_adi);
      intent.putExtra("ex_kategori_adi",kategori_adi);
      intent.putExtra("ex_ilce",ilce);
      intent.putExtra("ex_ucret",ucret);
      startActivity(intent);
    }


    @Override
    public void onClick(View view) {
        if(view.equals(KitapAraImageViewLogo)){
        Intent intent = new Intent(KitapAraActivity.this,AnasayfaActivity.class);
        startActivity(intent); }
        else if(view.equals(KitapAraTextViewCikis)){
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //BENİ HATIRLAYI BOŞALTIYORUZ---------------------------------------

                            session.setPrefDurum(false);
                            session.setPrefID(0);
                            session.setPrefUsername("");
                            session.setPrefName("");
                            session.setPrefEposta("");
                            session.setPrefGuncellemeTarihi("");
                            session.setPrefIlce("");
                            session.setPrefKayitTarihi("");
                            session.setPrefRolID(0);
                            session.setPrefSehir("");
                            session.setPrefSifre("");
                            session.setPrefTelefon("");
                            //-------------------------------------------------------------
                            loginSayfasinaGit();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(KitapAraActivity.this);
            builder.setMessage("Çıkmak İstediğinize Emin Misiniz ?").setPositiveButton("Evet", dialogClickListener)
                    .setNegativeButton("Hayır", dialogClickListener).show();

        }
    }
    private void loginSayfasinaGit() {
        Intent intent = new Intent(KitapAraActivity.this,MainActivity.class);
        startActivity(intent);

    }


    private void KategorilereIstekAt() {


        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Bilgi");
        progressDialog.setMessage("Yükleniyor");
        progressDialog.setCancelable(false);
        progressDialog.show();


        KategorilerService ks = APIClient.getClient().create(KategorilerService.class);
        Call<List<Kategoriler>> bilgiList = ks.findAll( APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());
        bilgiList.enqueue(new Callback<List<Kategoriler>>() {
            @Override
            public void onResponse(Call<List<Kategoriler>> call, Response<List<Kategoriler>> response) {

                if(response.isSuccessful())
                {
                    list=response.body();

                    progressDialog.cancel();

                    listecik_isim = new ArrayList<String>();
                    listecik_id = new ArrayList<Integer>();


                    for(int i=0;i<list.size();i++)
                    {
                        Kategoriler jObj = list.get(i);
                        listecik_isim.add(jObj.getKategoriAdi());
                        listecik_id.add(jObj.getID());
                        int a = 5;


                    }
                    ArrayAdapter<String> adp1 = new ArrayAdapter<String>(KitapAraActivity.this,
                            android.R.layout.simple_list_item_1, listecik_isim);
                    adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    KitapAraSpinnerKategoriler.setAdapter(adp1);
                    KitapAraSpinnerKategoriler.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);


                }
                else
                {
                    progressDialog.cancel();
                    // Toast.makeText(TumKullanicilarActivity.this, response.message().toString() , Toast.LENGTH_SHORT).show();

                }



            }

            @Override
            public void onFailure(Call<List<Kategoriler>> call, Throwable t) {

            }
        });



    }
}
