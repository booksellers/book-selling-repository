package com.example.user.mobilodevi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

public class SifreSorActivity extends AppCompatActivity {

    EditText sifre ;
    Session ses;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sifre_sor);
        ses=new Session(getApplicationContext());
       sifre=findViewById(R.id.editTextSifreSifre);
      final String gelen = ses.getPrefSifre();
gelen.toString();
        sifre.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if((""+charSequence.toString()).equals(""+gelen.toString()))
                {

                    Intent intent = new Intent(SifreSorActivity.this,ProfilimActivity.class);
                    startActivity(intent);


                }
            }

            @Override
            public void afterTextChanged(Editable editable) {



            }
        });
    }
}
