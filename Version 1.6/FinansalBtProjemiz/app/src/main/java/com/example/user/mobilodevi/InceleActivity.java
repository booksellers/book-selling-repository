package com.example.user.mobilodevi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mobilodevi.Adapters.ViewPagerAdapter;
import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Kategoriler;
import com.example.user.mobilodevi.Entities.Kullanicilar;
import com.example.user.mobilodevi.Services.KategorilerService;
import com.example.user.mobilodevi.Services.KullanicilarService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InceleActivity extends AppCompatActivity {

    ViewPager vp;
    ViewPagerAdapter vpa;
    String[] images;
    String kullanici_adi,yazar , kitap_adi , yayin_evi,ucret , resim1 , resim2,telefon,il,ilce,aciklama,kategor_adi,ilan_sahibi,kategori_adi;
    Integer ilan_id,tur_id,kategori_id,ilan_sahibi_id;
    TextView tv_ilan_id,tv_kitap_adi,tv_kategori , tv_yayinevi , tv_yazar , tv_ucret , tv_aciklma , tv_il , tv_ilce , tv_adres , tv_ilansahibi;
    Session session;
    ProgressDialog progress ;
    ImageView InceleImageViewLogo;
    TextView InceleTextViewCikis, InceleTextViewAdSoyad;








    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incele);



        session=new Session(getApplicationContext());
        Tanimla();

        Intent intent = getIntent();


         InceleTextViewAdSoyad.setText(session.getPrefName());
        resim1 = intent.getStringExtra("incele_resim1");
        resim2 = intent.getStringExtra("incele_resim2");
        images = new String[]{resim1,resim2};

       vp=findViewById(R.id.incele_ViewPager);
        vpa = new ViewPagerAdapter(InceleActivity.this,images);
        vp.setAdapter(vpa);


        seciliKategoriGetir();
        seciliKullaniciGetir();

             kitap_adi =  intent.getStringExtra("incele_kitapadi");
       yayin_evi = intent.getStringExtra("incele_yayinevi");
        ucret  = intent.getStringExtra("incele_ucret");
        kategori_id = Integer.parseInt(intent.getStringExtra("incele_kategori_id"));
        yazar = intent.getStringExtra("incele_yazar");

        telefon = intent.getStringExtra("incele_telefon");
        il = intent.getStringExtra("incele_il");
        ilce = intent.getStringExtra("incele_ilce");
        aciklama = intent.getStringExtra("incele_aciklama");
        ilan_sahibi_id = Integer.parseInt(intent.getStringExtra("incele_ilansahibi_id"));


        IlkDegerleriYerlestir();
        progress = new ProgressDialog(InceleActivity.this);

        progress.setTitle("Lütfen Bekleyiniz");
        progress.setMessage("İlan Veriliyor...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog








    }




    private void seciliKategoriGetir() {

        kategori_id = Integer.parseInt(getIntent().getStringExtra("incele_kategori_id"));
        KategorilerService ks = APIClient.getClient().create(KategorilerService.class);
        final Call<Kategoriler> bilgi = ks.find(kategori_id,APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Bilgi");
        progressDialog.setMessage("Yükleniyor");
        progressDialog.setCancelable(false);
        progressDialog.show();
        bilgi.enqueue(new Callback<Kategoriler>() {
            @Override
            public void onResponse(Call<Kategoriler> call, Response<Kategoriler> response) {
                if(response.isSuccessful())
                {
                    kategori_adi = response.body().getKategoriAdi();
                    tv_kategori.setText("Kategori : "+kategori_adi.toString());

                }

                else
                {
                }
                progressDialog.cancel();
            }

            @Override
            public void onFailure(Call<Kategoriler> call, Throwable t) {
            }
        });
        progressDialog.cancel();
    }
    private void seciliKullaniciGetir() {

        ilan_sahibi_id = Integer.parseInt(getIntent().getStringExtra("incele_ilansahibi_id"));
        KullanicilarService ks = APIClient.getClient().create(KullanicilarService.class);
        final Call<Kullanicilar> bilgi = ks.find(ilan_sahibi_id,APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Bilgi");
        progressDialog.setMessage("Yükleniyor");
        progressDialog.setCancelable(false);
        progressDialog.show();

        bilgi.enqueue(new Callback<Kullanicilar>() {
            @Override
            public void onResponse(Call<Kullanicilar> call, Response<Kullanicilar> response) {
                if(response.isSuccessful())
                {
                    kullanici_adi = ""+response.body().getAdSoyad().toString();
                    tv_ilansahibi.setText("Ilan Sahibi : "+kullanici_adi.toString());

                }

                else
                {
                }
                progressDialog.cancel();
            }

            @Override
            public void onFailure(Call<Kullanicilar> call, Throwable t) {
                progressDialog.cancel();

            }

        });
    }



    public void msjclck(View v)
    {
        Toast.makeText(this, "Uzgunuz Mesajlasma Hizmetimiz Henuz Kullanimda Degildir :(", Toast.LENGTH_SHORT).show();

    }

    private void Tanimla() {

        InceleImageViewLogo=(ImageView)findViewById(R.id.InceleImageViewLogo);
        InceleTextViewCikis=(TextView)findViewById(R.id.InceleTextViewCikis);
        InceleTextViewAdSoyad=(TextView)findViewById((R.id.InceleTextViewAdSoyad));
        tv_ilan_id = findViewById(R.id.incele_TextViewID);
        tv_kitap_adi = findViewById(R.id.incele_TextViewKitapAdi);
        tv_kategori = findViewById(R.id.incele_TextViewKategori);
        tv_yayinevi = findViewById(R.id.incele_TextViewyayinEvi);
        tv_yazar = findViewById(R.id.incele_TextViewYazar);
        tv_ucret = findViewById(R.id.incele_TextViewUcret);
        tv_aciklma = findViewById(R.id.incele_TextViewAciklama);
        tv_il = findViewById(R.id.incele_TextViewIl);
        tv_ilce = findViewById(R.id.inceleTextViewIlce);
        tv_adres = findViewById(R.id.incele_TextViewAdres);
         tv_ilansahibi = findViewById(R.id.incele_TextViewIlanSahibi);
    }

    private void IlkDegerleriYerlestir() {

        tv_kitap_adi.setText("Kitap Adı  : "+kitap_adi);
     //   tv_kategori.setText( "Kategori   : "+kategor_adi);
        tv_yayinevi.setText( "Yayın Evi  : "+yayin_evi);
        tv_yazar.setText(    "Yazar      : "+yazar);      //yazar bilgisi gelecek
        tv_ucret.setText(    "Ucret      : "+ucret);
        tv_aciklma.setText(  "Aciklama   : "+aciklama);
        tv_il.setText(       "Sehir      : "+il);
        tv_ilce.setText(     "Ilce       : "+ilce);
        tv_adres.setText(    "Telefon    : "+telefon);
    //    tv_ilansahibi.setText("İlan Sahibi: "+ilan_sahibi);




    }


    public void geriClick(View v)
    {
        Intent intent = new Intent(InceleActivity.this,IlanlarimActivity.class);
        startActivity(intent);






    }
}
