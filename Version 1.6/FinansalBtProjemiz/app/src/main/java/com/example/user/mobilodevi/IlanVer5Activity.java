package com.example.user.mobilodevi;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mobilodevi.Adapters.ViewPagerAdapter;
import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Ilanlar;
import com.example.user.mobilodevi.Entities.Kullanicilar;
import com.example.user.mobilodevi.Services.IlanlarService;
import com.example.user.mobilodevi.Services.KullanicilarService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IlanVer5Activity extends AppCompatActivity implements View.OnClickListener{
//ÖNEMLİ NOT ADRES YERİNE TELEFON YAZDIRIYORUZ GEÇİÇİ BİR SÜRE İÇİN
    ViewPager vp;
    ViewPagerAdapter vpa;
    String[] images;
    String yazar , kitap_adi , yayin_evi,ucret , resim1 , resim2,adres,il,ilce,aciklama,kategor_adi,ilan_sahibi;
    Integer tur_id,kategori_id,ilan_sahibi_id;
    TextView tv_kitap_adi,tv_kategori , tv_yayinevi , tv_yazar , tv_ucret , tv_aciklma , tv_il , tv_ilce , tv_adres , tv_ilansahibi;
    Session session;
    ProgressDialog progress ;
    ImageView KitapAraImageViewLogo;
    TextView KitapAraTextViewCikis, KitapAraTextViewAdSoyad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilan_ver5);
         session=new Session(getApplicationContext());
         Tanimla();
        KitapAraImageViewLogo=(ImageView)findViewById(R.id.KitapAraImageViewLogo);
        KitapAraImageViewLogo.setOnClickListener(this);
        KitapAraTextViewCikis=(TextView)findViewById(R.id.KitapAraTextViewCikis);
        KitapAraTextViewCikis.setOnClickListener(this);
        KitapAraTextViewAdSoyad=(TextView)findViewById((R.id.KitapAraTextViewAdSoyad));
        KitapAraTextViewAdSoyad.setText(session.getPrefName());
        Intent intent = getIntent();
        resim1 = intent.getStringExtra("intent_ilanver_resim1");
        resim2 = intent.getStringExtra("intent_ilanver_resim2");
        images = new String[]{resim1,resim2};

        vp=findViewById(R.id.ilanVer5_ViewPager);
        vpa = new ViewPagerAdapter(IlanVer5Activity.this,images);
        vp.setAdapter(vpa);
  //DİĞER SAYFALARDA NEXT DEDİĞİMZDE GELENİNENTLERDEKİ BİLGİLER YERLEŞTİRİLİT
        yazar = intent.getStringExtra("intent_ilanver_durum");
        kitap_adi =  intent.getStringExtra("intent_ilanver_kitap_adi");
        yayin_evi = intent.getStringExtra("intent_ilanver_yayin_evi");
        ucret  = intent.getStringExtra("intent_ilanver_ucret");
        tur_id =  Integer.parseInt(intent.getStringExtra("intent_ilanver_tur_id"));
        kategori_id = Integer.parseInt(intent.getStringExtra("intent_ilanver_kitap_kategori_id"));

        adres = intent.getStringExtra("intent_ilanver_adres");
        il = intent.getStringExtra("intent_ilanver_il");
        ilce = intent.getStringExtra("intent_ilanver_ilce");
        aciklama = intent.getStringExtra("intent_ilanver_aciklama");
        kategor_adi = intent.getStringExtra("intent_ilanver_kitap_kategori_adi");
        ilan_sahibi = session.getPrefName();
        ilan_sahibi_id = session.getPrefID();

     //   Toast.makeText(this, yazar + " " + kitap_adi +" " + yayin_evi +" " + ucret +" " + tur_id + " " + kategori_id +" "+resim1+" "+resim2
       //         +" " + adres + " " + il + " " + ilce + " " + aciklama, Toast.LENGTH_LONG).show();

        IlkDegerleriYerlestir();
        progress = new ProgressDialog(IlanVer5Activity.this);

        progress.setTitle("Lütfen Bekleyiniz");
        progress.setMessage("İlan Veriliyor...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog

    }

    private void Tanimla() {

        tv_kitap_adi = findViewById(R.id.Ilanver5TextViewKitapAdi);
        tv_kategori = findViewById(R.id.Ilanver5TextViewKategori);
        tv_yayinevi = findViewById(R.id.Ilanver5TextViewyayinEvi);
        tv_yazar = findViewById(R.id.Ilanver5TextViewYazar);
        tv_ucret = findViewById(R.id.Ilanver5TextViewUcret);
        tv_aciklma = findViewById(R.id.Ilanver5TextViewAciklama);
        tv_il = findViewById(R.id.Ilanver5TextViewIl);
        tv_ilce = findViewById(R.id.Ilanver5TextViewIlce);
        tv_adres = findViewById(R.id.Ilanver5TextViewAdres);
        tv_ilansahibi = findViewById(R.id.ilanver5TextViewIlanSahibi);
    }

    private void IlkDegerleriYerlestir() {

        tv_kitap_adi.setText("Kitap Adı  : "+kitap_adi);
        tv_kategori.setText( "Kategori   : "+kategor_adi);
        tv_yayinevi.setText( "Yayın Evi  : "+yayin_evi);
        tv_yazar.setText(    "Yazar      : "+yazar);      //yazar bilgisi gelecek
        tv_ucret.setText(    "Ucret      : "+ucret);
        tv_aciklma.setText(  "Aciklama   : "+aciklama);
        tv_il.setText(       "Sehir      : "+il);
         tv_ilce.setText(     "Ilce       : "+ilce);
         tv_adres.setText(    "Telefon    : "+adres);
        tv_ilansahibi.setText("İlan Sahibi: "+ilan_sahibi);




    }

    public void ilaniyayinla(View v)
    {
        ilanVerIstek();
        Intent intent = new Intent(IlanVer5Activity.this,AnasayfaActivity.class);
        startActivity(intent);
    }

    private void ilanVerIstek() {  //İLAN BURADA VERİLİR



        try {
            Ilanlar Verilecek_Ilan = new Ilanlar();
            Verilecek_Ilan.setAciklama(aciklama);
            Verilecek_Ilan.setAdres(adres);
            //Verilecek_Ilan.setID();
            Verilecek_Ilan.setIlanTarihi("2018-01-01");
            Verilecek_Ilan.setIlce(ilce);
            Verilecek_Ilan.setKategoriID(kategori_id);
            Verilecek_Ilan.setKitapAdi(kitap_adi);
            Verilecek_Ilan.setKullaniciID(ilan_sahibi_id);
            if (resim1.trim().equals(""))
                resim1="resimyok";
            if (resim2.trim().equals(""))
                resim2="resimyok";
            Verilecek_Ilan.setResim(resim1);
            Verilecek_Ilan.setResim2(resim2);
            Verilecek_Ilan.setSehir(il);
            Verilecek_Ilan.setTurID(tur_id);
            Verilecek_Ilan.setUcret(ucret);
            Verilecek_Ilan.setYayinEvi(yayin_evi);
            Verilecek_Ilan.setYazar(yazar);


            progress.show();
            IlanlarService ilanServis = APIClient.getClient().create(IlanlarService.class);      //APIYE ISTEK ATILIR
            Call call = ilanServis.create(Verilecek_Ilan,APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());  //SERVICESE nesne yollanır
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    if(response.isSuccessful())
                    {
                        progress.cancel();
                        Toast.makeText(IlanVer5Activity.this, "İlaniniz Basariyla Yayinlanmistir ", Toast.LENGTH_SHORT).show();


                    }
                    else
                    {
                        progress.cancel();
                        Toast.makeText(IlanVer5Activity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    progress.cancel();
                    Toast.makeText(IlanVer5Activity.this, t.getMessage() , Toast.LENGTH_SHORT).show();
                }
            });


        }
        catch (Exception e)
        {

            Toast.makeText(this, e.getMessage() , Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onClick(View view) {
        if(view.equals(KitapAraImageViewLogo)){
            Intent intent = new Intent(IlanVer5Activity.this,AnasayfaActivity.class);
            startActivity(intent); }
        else if(view.equals(KitapAraTextViewCikis)){
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //BENİ HATIRLAYI BOŞALTIYORUZ---------------------------------------

                            session.setPrefDurum(false);
                            session.setPrefID(0);
                            session.setPrefUsername("");
                            session.setPrefName("");
                            session.setPrefEposta("");
                            session.setPrefGuncellemeTarihi("");
                            session.setPrefIlce("");
                            session.setPrefKayitTarihi("");
                            session.setPrefRolID(0);
                            session.setPrefSehir("");
                            session.setPrefSifre("");
                            session.setPrefTelefon("");
                            //-------------------------------------------------------------
                            loginSayfasinaGit();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(IlanVer5Activity.this);
            builder.setMessage("Cikmak Istediginize Emin Misiniz ?").setPositiveButton("Evet", dialogClickListener)
                    .setNegativeButton("Hayır", dialogClickListener).show();

        }
    }
    private void loginSayfasinaGit() {
        Intent intent = new Intent(IlanVer5Activity.this,MainActivity.class);
        startActivity(intent);

    }

}
