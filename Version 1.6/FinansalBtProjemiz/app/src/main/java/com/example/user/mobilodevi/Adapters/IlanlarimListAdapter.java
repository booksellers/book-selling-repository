package com.example.user.mobilodevi.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Ilanlar;
import com.example.user.mobilodevi.Entities.Kullanicilar;
import com.example.user.mobilodevi.IlanDuzenleActivity;
import com.example.user.mobilodevi.IlanlarimActivity;
import com.example.user.mobilodevi.MainActivity;
import com.example.user.mobilodevi.ProfilimActivity;
import com.example.user.mobilodevi.R;
import com.example.user.mobilodevi.Services.IlanlarService;
import com.example.user.mobilodevi.Services.KullanicilarService;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IlanlarimListAdapter extends BaseAdapter {

    List<Ilanlar> list;
    Context context;
    Activity activity;
    ProgressDialog progress ;


    public IlanlarimListAdapter(List<Ilanlar> list, Context context , Activity actvity) {


        this.list = list;
        this.context = context;
        this.activity=actvity;
        progress = new ProgressDialog(activity);
        progress.setTitle("Lütfen Bekleyiniz");
        progress.setMessage("İşlem Yapılıyor ...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view= LayoutInflater.from(context).inflate(R.layout.ilanlarim_alt_sayfa,viewGroup,false);
        LinearLayout kompleLayout;
        kompleLayout = view.findViewById(R.id.KompleLayoutAltSayfa);
        TextView tv_id=(TextView)view.findViewById(R.id.IlanlarimAltSayfaTextViewSQID);
        TextView tv_kitap_adi=(TextView)view.findViewById(R.id.IlanlarimAltSayfaTextViewKitapAdi);
        TextView tv_kategori=(TextView)view.findViewById(R.id.IlanlarimAltSayfaTextViewKategori);
        TextView tv_sehir=(TextView)view.findViewById(R.id.IlanlarimAltSayfaTextViewSehir);
        TextView tv_ilce=(TextView)view.findViewById(R.id.IlanlarimAltSayfaTextViewIlce);
        ImageView img = view.findViewById(R.id.IlanlarimAltSayfaImageViewResim);
        Button btn_duzenle = view.findViewById(R.id.IlanlarimAltSayfaButtonDuzenle);
        final int di=i;



        btn_duzenle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {  //DUZENLEYE BASINCA INTENTLERI ALIP YOLLAR

                Intent intent = new Intent(activity,IlanDuzenleActivity.class);
                intent.putExtra("duzenle_ilan_id",""+list.get(di).getID());
                intent.putExtra("duzenle_ilan_kitapadi",""+list.get(di).getKitapAdi());
                intent.putExtra("duzenle_ilan_kategoriid",""+list.get(di).getKategoriID());
                intent.putExtra("duzenle_ilan_yayinevi",""+list.get(di).getYayinEvi());
                intent.putExtra("duzenle_ilan_ucret",""+list.get(di).getUcret());
                intent.putExtra("duzenle_ilan_yazar",""+list.get(di).getYazar());
                intent.putExtra("duzenle_ilan_aciklama",""+list.get(di).getAciklama());
                intent.putExtra("duzenle_ilan_ilansahibi_id",""+list.get(di).getKullaniciID());
                intent.putExtra("duzenle_ilan_il",""+list.get(di).getSehir());
                intent.putExtra("duzenle_ilan_ilce",""+list.get(di).getIlce());
                intent.putExtra("duzenle_ilan_resim1",""+list.get(di).getResim());
                intent.putExtra("duzenle_ilan_resim2",""+list.get(di).getResim2());
                intent.putExtra("duzenle_ilan_adres",""+list.get(di).getAdres()); //TELEFON
                activity.startActivity(intent);



            }
        });
        Button btn_sil = view.findViewById(R.id.IlanlarimAltSayfaButtonSil);
         btn_sil.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {  //SİLME İŞLEMİ BURADA

                 DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                         switch (which){
                             case DialogInterface.BUTTON_POSITIVE:
                                 try {

                                     IlanlarService ilanService = APIClient.getClient().create(IlanlarService.class);  //APİ İSTEK ATILIR
                                     Call call = ilanService.delete(list.get(di).getID() ,APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());


                                     progress.show();
                                     call.enqueue(new Callback() {
                                         @Override
                                         public void onResponse(Call call, Response response) {
                                             progress.cancel();
                                             activity.startActivity(new Intent(activity, IlanlarimActivity.class));

                                         }

                                         @Override
                                         public void onFailure(Call call, Throwable t) {
                                             progress.cancel();
                                             Toast.makeText(activity, "Hata Oluştu", Toast.LENGTH_SHORT).show();
                                         }
                                     });






                                 }
                                 catch (Exception e)
                                 {
                                     progress.cancel();
                                     Toast.makeText(activity, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                                 }



                                 break;

                             case DialogInterface.BUTTON_NEGATIVE:
                                 //No button clicked
                                 break;
                         }
                     }
                 };

                 AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                 builder.setMessage("İlanınızı Silmek  İstediğinize Emin Misiniz ?").setPositiveButton("Evet", dialogClickListener)
                         .setNegativeButton("Hayır", dialogClickListener).show();
             }
         });


        final int sq_id = list.get(i).getID();
        final String kitap_adi = list.get(i).getKitapAdi();
        final String kategori = ""+ list.get(i).getAdres();
        final String sehir = ""+list.get(i).getSehir();
        final String ilce = list.get(i).getIlce();
        final String resim = list.get(i).getResim().toString();
        tv_id.setText(""+sq_id+" NUMARALI ILAN");
        tv_kitap_adi.setText("Kitap Adı :"+kitap_adi);
        tv_kategori.setText("İletisim : "+kategori);
        tv_sehir.setText("İl :"+sehir);
        tv_ilce.setText("İlce : "+ilce);
        Picasso.with(activity.getApplicationContext()).load(resim).placeholder(R.drawable.yukleniyor).error(R.drawable.resimbulunamadi).into(img);






        return view;
    }



}
